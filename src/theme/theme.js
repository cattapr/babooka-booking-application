const theme = {
    colors: {
        disabled: '#D3D3D3',
        primary: '#ffbf00',
        primary2: '#414141',
        primary3: '#ececec',
        primary4: '#4a90e2',
        primary5: '#3d3d3d',
        black: '#000000',
        white: '#ffffff',
    },

    fontSizes: {
        //assuming base font-size is 10px
        medium: '2rem', // 20px
        big: '2.5rem', // 20px
        xs: '1.2rem', // 12px
        sm: '1.4rem', // 14px
        lg: '1.6rem', // 16px
        xl: '3.5rem', // 35px
    },

    fontWeights: {
        thin: 200,
        normal: 400,
        semibold: 600,
        bold: 800,
        extrabold: 900
    },

    borderWidths: {
        default: '1px',
        '2': '2px'
    },

    borderRadius: {
        xs: '6px',
        sm: '10px',
        m: '15px',
        lg: '20px'
    },

    width: {
        contentWrapper: '125rem',
        sm: '10rem',
        lg: '80rem',
        full: '100%',
    },

    height: {
        default: '4rem',
        xs: '7rem',
        sm: '10rem',
        md: '18rem',
        lg: '40rem'
    },


    padding: {
        default: '1.5rem',
        xs: '0.6rem',
        sm: '0.8rem',
        md: '2.3rem',
        lg: '10rem',
        '1': '1rem',
        '2': '2rem',
        '3': '3rem',
        '4': '4rem',
        '5': '5rem',
        '7': '7rem'
    },

    margin: {
        auto: 'auto',
        xs: '1.2rem',
        '1': '1rem',
        '2': '2rem',
        '3': '3rem',
        '5': '5rem',
    },

    shadows: {
        default: '2px 2px 10px 0 rgba(0, 0, 0, 0.08)',
        sm: '1px 3px 15px 0 rgba(0, 0, 0, 0.2)',
        md: '0 2px 5px 0 rgba(0, 0, 0, 0.08)',
        lg: '0 2px 10px 0 rgba(0, 0, 0, 0.08)'
    },

    lineHeight: {
        sm: '2.5rem'
    }

}

export default theme;

