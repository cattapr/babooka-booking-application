import React from 'react';
import ActivityList from '../../components/ActivityList';
import CategoryList from '../../components/CategoryList/CategoryList';
import Hero from '../../components/Hero';

const Activities = () => {
    return (
        <>
            <Hero />
            <CategoryList />
            <ActivityList />
        </>
    );
}

export default Activities;
