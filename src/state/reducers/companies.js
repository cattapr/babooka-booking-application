import { GET_COMPANIES_ERROR, GET_COMPANIES_START, GET_COMPANIES_SUCCESS } from '../actions/action-types';

const initialState = {
    load: false,
    error: false,
    companies: []
};


const companies = (state = initialState, action) => {
    switch (action.type) {
        case GET_COMPANIES_START:
            return { ...state, load: true };

        case GET_COMPANIES_SUCCESS:
            return {
                ...state,
                load: false,
                companies: action.payload.companies
            };

        case GET_COMPANIES_ERROR:
            return {
                ...state,
                load: false,
                error: true
            };

        default:
            return state;
    }
};

export default companies;

