import { combineReducers } from 'redux';
import activities from './activities';
import categories from './categories';
import companies from './companies';
import filter from './filter';

export default combineReducers({ activities, categories, filter, companies });