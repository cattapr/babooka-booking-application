import { GET_CATEGORIES_ERROR, GET_CATEGORIES_START, GET_CATEGORIES_SUCCESS } from '../actions/action-types';

const initialState = {
    loading: false,
    error: false,
    categories: []
};


const categories = (state = initialState, action) => {
    switch (action.type) {
        case GET_CATEGORIES_START:
            return { ...state, loading: true };

        case GET_CATEGORIES_SUCCESS:
            return {
                ...state,
                loading: false,
                categories: action.payload.categories
            };

        case GET_CATEGORIES_ERROR:
            return {
                ...state,
                loading: false,
                error: true
            };

        default:
            return state;
    }
};

export default categories;

