import { REMOVE_FILTER, SET_FILTER } from '../actions';

const initialState = {
    category: null,
    searchValue: "",
    active: false

};

const filter = (state = initialState, action) => {
    switch (action.type) {
        case SET_FILTER:
            const { searchValue, category } = action.payload;
            return {
                ...state,
                active: true,
                category: category || state.category,
                searchValue: searchValue !== undefined ? searchValue : state.searchValue
            };

        case REMOVE_FILTER:
            const { type } = action.payload;
            return {
                ...state,
                active: true,
                category: type === "category" ? "" : state.category,
                searchValue: type === "searchValue" ? "" : state.searchValue,
            };


        default:
            return state
    }
}

export default filter;