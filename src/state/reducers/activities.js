import { GET_ACTIVITIES_ERROR, GET_ACTIVITIES_START, GET_ACTIVITIES_SUCCESS, UPDATE_FILTER_ACTIVITIES } from '../actions/action-types';

const initialState = {
    loading: false,
    error: false,
    activities: [],
    filteredActivities: []
};


const activities = (state = initialState, action) => {
    switch (action.type) {
        case GET_ACTIVITIES_START:
            return { ...state, loading: true };

        case GET_ACTIVITIES_SUCCESS:
            return {
                ...state, loading: false,
                activities: action.payload.activities,
                filteredActivities: action.payload.activities
            };

        case GET_ACTIVITIES_ERROR:
            return {
                ...state,
                loading: false,
                error: true
            };

        case UPDATE_FILTER_ACTIVITIES:

            let filteredActivities = [...state.activities];
            const { companies, filter } = action.payload;

            if (filter.category) {
                filteredActivities = filteredActivities.filter(activity => activity.categoryIds.includes(filter.category.id))
            }

            if (filter.searchValue) {
                filteredActivities = filteredActivities.filter(activity => {
                    const nameExist = activity.name.toLowerCase().includes(filter.searchValue);

                    const company = companies.find(
                        (company) => company.id === activity.companyId
                    );

                    const companyExist = company.name.toLowerCase().includes(filter.searchValue);

                    return nameExist || companyExist;
                });
            }

            return {
                ...state,
                filteredActivities
            };

        default:
            return state;
    }
};

export default activities;

