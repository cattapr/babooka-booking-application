import { GET_ACTIVITIES_ERROR, GET_ACTIVITIES_START, GET_ACTIVITIES_SUCCESS, GET_CATEGORIES_ERROR, GET_CATEGORIES_START, GET_CATEGORIES_SUCCESS, GET_COMPANIES_ERROR, GET_COMPANIES_START, GET_COMPANIES_SUCCESS, REMOVE_FILTER, SET_FILTER, UPDATE_FILTER_ACTIVITIES } from './action-types';


const getActivitiesStart = () => ({
    type: GET_ACTIVITIES_START
});

const getActivitiesSuccess = activities => ({
    type: GET_ACTIVITIES_SUCCESS,
    payload: { activities }
});

const getActivitiesError = error => ({
    type: GET_ACTIVITIES_ERROR,
    payload: { error }
});


export const updateFilteredActivities = (companies, filter) => ({
    type: UPDATE_FILTER_ACTIVITIES,
    payload: { companies, filter }
});

export const fetchActivities = () => {
    return (dispatch) => {
        dispatch(getActivitiesStart())
        fetch('https://babooka-api.now.sh/activities')
            .then((response) => {
                return response.json();
            })
            .then((activities) => {
                dispatch(getActivitiesSuccess(activities));
            })
            .catch((error) => {
                dispatch(getActivitiesError(error))
            })
    };
}

const getCategoriesStart = () => ({
    type: GET_CATEGORIES_START
});

const getCategoriesSuccess = categories => ({
    type: GET_CATEGORIES_SUCCESS,
    payload: { categories }
});

const getCategoriesError = error => ({
    type: GET_CATEGORIES_ERROR,
    payload: { error }
});


export const fetchCategories = () => {
    return (dispatch) => {
        dispatch(getCategoriesStart());
        fetch('https://babooka-api.now.sh/categories')
            .then((response) => {
                return response.json();
            })
            .then((categories) => {
                dispatch(getCategoriesSuccess(categories))
            })
            .catch((error) => {
                dispatch(getCategoriesError(error))
            })
    };
}

const getCompaniesStart = () => ({
    type: GET_COMPANIES_START
});

const getCompaniesSuccess = companies => ({
    type: GET_COMPANIES_SUCCESS,
    payload: { companies }
});

const getCompaniesError = error => ({
    type: GET_COMPANIES_ERROR,
    payload: { error }
});


export const fetchCompanies = () => {
    return (dispatch) => {
        dispatch(getCompaniesStart());
        fetch('https://babooka-api.now.sh/companies')
            .then((response) => {
                return response.json();
            })
            .then((companies) => {
                dispatch(getCompaniesSuccess(companies))
            })
            .catch((error) => {
                dispatch(getCompaniesError(error))
            })
    };
}



export const setFilter = ({ searchValue, category }) => {
    return {
        type: SET_FILTER,
        payload: {
            searchValue,
            category
        }
    };
}



export const removeFilter = (type) => {
    return {
        type: REMOVE_FILTER,
        payload: {
            type
        }
    };
}

