import { library } from '@fortawesome/fontawesome-svg-core';
import { faBroom, faCut, faDumbbell, faHandHoldingHeart, faHandPaper, faHotTub, faIdCard, faSearch, faShoePrints, faTimes } from '@fortawesome/free-solid-svg-icons';
import React from 'react';
import ReactDOM from 'react-dom';
import { ThemeProvider } from 'react-jss';
import { Provider } from 'react-redux';
import './index.css';
import Routing from './Routing';
import * as serviceWorker from './serviceWorker';
import store from './state/store';
import theme from './theme/theme.js';


library.add(faTimes, faBroom, faCut, faDumbbell, faHandHoldingHeart, faHandPaper, faHotTub, faIdCard, faSearch, faShoePrints)

ReactDOM.render(
    <ThemeProvider theme={theme}>
        <Provider store={store}>
            <Routing />
        </Provider>
    </ThemeProvider>
    , document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
