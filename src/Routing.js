import { Router } from '@reach/router';
import React from 'react';
import Footer from './components/Footer';
import Header from './components/Header';
import About from './pages/About/About';
import Activities from './pages/Activities';
import Companies from './pages/Companies';
import Facilities from './pages/Facilities';


const Routing = () => {
  return (
    <div>
      <Header />
      <main>
        <Router>
          <Activities path="/" />
          <Facilities path="/facilities" />
          <About path="/about-us" />
          <Companies path="/companies" />
        </Router>
      </main>
      <Footer />
    </div>
  );
};

export default Routing;
