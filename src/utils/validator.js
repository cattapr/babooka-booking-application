export const validator = values => {
    const errors = {};

    if (Object.keys(values).length > 0) {
        if (!values.name) {
            errors.name = "Vänlingen fyll i fält"
        }

        if (!values.number) {
            errors.number = "Vänlingen fyll i fält"
        }

        if (values.number && values.number.toString().length > 10) {
            errors.number = "Vänlingen fyll ett 10 siffrigt nummer"
        }

        if (!values.email) {
            errors.email = "Vänlingen fyll i fält"
        }

        if (!values.availableTime) {
            errors.availableTime = "Vänlingen välj ett datum"
        }
    }
    return errors;
}
