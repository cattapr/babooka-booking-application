import React from 'react';
import { Field } from 'react-final-form';
import { createUseStyles } from 'react-jss';


const useStyles = createUseStyles(theme => ({
    inputField: {
        padding: `${theme.padding['1']} ${theme.padding['2']}`,
        margin: theme.margin.xs,
        borderRadius: theme.borderRadius.m,
        boxShadow: theme.shadows.md,
        borderWidth: 0,
        flexGrow: 1,
    },

    errorMessage: {
        color: "#D8000C",
        backgroundColor: "#FFD2D2",
        padding: theme.padding['1'],
        borderRadius: theme.borderRadius.xs,
    }
}));

const InputField = ({ type, name, placeholder, labelText, value }) => {
    const classes = useStyles();
    return (
        <Field name={name} type={type} value={value}>
            {({ input, meta }) => (
                <>
                    <input id={name} className={classes.inputField} placeholder={placeholder} {...input} />
                    <label htmlFor={name} className="srOnly">{labelText}</label>
                    <span aria-live="assertive">
                        {meta.touched && meta.error && meta.submitFailed ? <span className={classes.errorMessage}>{meta.error}</span> : null}
                    </span>
                </>
            )}
        </Field>
    );
}

export default InputField;