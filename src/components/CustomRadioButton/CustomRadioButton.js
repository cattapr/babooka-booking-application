import React from 'react';
import { Field } from 'react-final-form';
import { createUseStyles } from 'react-jss';


const useStyles = createUseStyles(theme => ({
    inputField: {
        opacity: 0,
        position: 'absolute',
        '&:checked + label': {
            border: '3px solid grey'
        }
    },


    avaliableTimeListItem: {
        background: theme.colors.primary,
        borderRadius: theme.borderRadius.m,
        color: theme.colors.white,
        fontSize: theme.fontSizes.lg,
        fontWeight: theme.fontWeights.bold,
        padding: `${theme.padding['2']} 0`,
        cursor: 'pointer'
    },

    avaliableTimeContainer: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center'
    },

    errorMessage: {
        color: "#D8000C",
        backgroundColor: "#FFD2D2",
        padding: theme.padding['1'],
        borderRadius: theme.borderRadius.xs,
    }
}));

const CustomRadioButton = ({ type, name, booking, value }) => {
    const classes = useStyles();
    return (
        <Field name={name} type={type} value={value}>
            {({ input, meta }) => (
                <>
                    <input id={booking.id} className={classes.inputField} {...input} />
                    <label className={`${classes.avaliableTimeListItem} ${classes.avaliableTimeContainer}`} htmlFor={booking.id}>
                        <span>{booking.startTime} - {booking.endTime}</span>
                        <span>20 platser</span>
                    </label>
                    <span aria-live="assertive">
                        {meta.touched && meta.error && meta.submitFailed ? <span className={classes.errorMessage}>{meta.error}</span> : null}
                    </span>
                </>
            )}
        </Field>
    );
}

export default CustomRadioButton;