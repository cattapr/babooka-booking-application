import React, { useEffect, useState } from 'react';
import { createUseStyles, useTheme } from 'react-jss';
import { useDispatch, useSelector } from 'react-redux';
import { fetchActivities, fetchCompanies, updateFilteredActivities } from '../../state/actions';
import theme from '../../theme/theme';
import ActivityCard from '../ActivityCard';
import Button from '../Button';
import FilterTags from '../FilterTags';

const useStyles = createUseStyles({
    section: {
        width: theme.width.contentWrapper,
        margin: `${theme.margin['5']} ${theme.margin.auto}`
    },

    totalActivities: {
        display: 'flex',
        justifyContent: 'flex-end',
        borderBottom: `${theme.borderWidths['default']} solid ${theme.colors.primary3}`,
        padding: theme.padding['2'],
        fontWeight: theme.fontWeights.bold,
        fontSize: theme.fontSizes.lg
    },

    listContainer: {
        display: 'grid',
        gridTemplateColumns: 'repeat(4, 1fr)',
        gridAutoRows: 'auto',
        gridGap: '3rem',
        padding: `${theme.padding['2']} 0`,
        listStyle: 'none',
        position: 'relative',
    },

    buttonContainer: {
        display: 'flex',
        justifyContent: 'center'
    }
});

const ActivityList = () => {
    const [showActivitiesAmount, setShowActivitiesAmount] = useState(9);
    const filter = useSelector(state => state.filter);
    const { loading, filteredActivities, activities } = useSelector(state => state.activities);
    const { load, companies } = useSelector(state => state.companies);
    const isDisabled = showActivitiesAmount >= filteredActivities.length;
    const classes = useStyles(isDisabled);
    const theme = useTheme();

    const dispatch = useDispatch();
    useEffect(
        () => {
            if (!loading && !activities.length) {
                dispatch(fetchActivities());
            }
        },
        [activities, loading, dispatch]
    );

    useEffect(
        () => {
            if (!load && !companies.length) {
                dispatch(fetchCompanies());
            }
        },
        [companies, load, dispatch]
    );

    useEffect(
        () => {
            dispatch(updateFilteredActivities(companies, filter));
        },
        [companies, filter, dispatch]
    );

    if (!activities.length || !companies.length) {
        return <div>Loading</div>;
    }
    return (
        <section className={classes.section}>
            <div>
                <FilterTags />
                <span className={classes.totalActivities}>{filteredActivities.length} resultat</span>
            </div>
            <ul className={classes.listContainer}>
                {filteredActivities.slice(0, showActivitiesAmount).map(activity => {
                    return <ActivityCard key={activity.id} activity={activity} />
                })}
            </ul>
            <div className={classes.buttonContainer}>
                <Button ariaLabelText="Visa mer aktiviteter" extraCSS={{
                    backgroundColor: isDisabled ? theme.colors.disabled : theme.colors.primary4,
                    color: theme.colors.white,
                    padding: `${theme.padding['1']} ${theme.padding['5']}`,
                }}
                    onClick={() => setShowActivitiesAmount(showActivitiesAmount + 9)} disabled={isDisabled}>Visa fler</Button>
            </div>
        </section >
    );
}

export default ActivityList;
