import React, { useState } from 'react';
import { Form } from 'react-final-form';
import { createUseStyles, useTheme } from 'react-jss';
import { validator } from '../../utils/validator';
import Button from '../Button';
import AvailableTimeSection from '../Modal/AvailableTimeSection';
import ContactInfo from '../Modal/ContactInfo';


const useStyles = createUseStyles(theme => ({
    formWrapper: {
        display: 'grid',
        gridTemplateColumns: '2fr 1fr',
        gridTemplateAreas: `"input textarea"`,
        gridGap: '5rem',
        background: theme.colors.primary,
        padding: theme.padding['4']
    },

    inputFieldContainer: {
        display: 'flex',
        flexDirection: 'column',
        gridArea: 'input'
    },

    inputField: {
        padding: `${theme.padding['1']} ${theme.padding['2']}`,
        margin: theme.margin.xs,
        borderRadius: theme.borderRadius.m,
        boxShadow: theme.shadows.md,
        borderWidth: 0,
        flexGrow: 1
    },

    textareaFieldGridArea: {
        display: 'flex',
        gridArea: 'textarea',
        alignItems: 'stretch'

    },

    buttonContainer: {
        display: 'flex',
        justifyContent: 'flex-end',
        alignItems: 'center',
        padding: theme.padding['3']
    },
    bookingMessage: {
        display: 'flex',
        justifyContent: 'center',
        fontWeight: theme.fontWeights.bold,
        fontSize: theme.fontSizes.lg
    }
}));


const BookingForm = ({ activity }) => {
    const classes = useStyles();
    const theme = useTheme();
    const [book, setBook] = useState(false);

    const bookActivity = async values => {
        setBook(true);
        localStorage.setItem('booking', JSON.stringify(values))
    }
    return (
        <Form
            onSubmit={bookActivity}
            validate={(values) => validator(values)}
            initialValues={{
                name: '',
                number: null,
                email: '',
                availableTime: ''
            }}
            render={({ handleSubmit, form }) => (
                <form onSubmit={(event) => {
                    const promise = handleSubmit(event);
                    promise && promise.then(() => {
                        form.reset();
                    })
                    return promise;

                }}>
                    <AvailableTimeSection activity={activity} />
                    <ContactInfo />
                    <div className={classes.buttonContainer}>
                        <Button
                            ariaLabelText="Boka aktivitet här"
                            type="submit"
                            extraCSS={{
                                backgroundColor: theme.colors.primary4,
                                color: theme.colors.white,
                                padding: `${theme.padding['1']} ${theme.padding['5']}`,
                            }}
                        > Boka </Button>
                    </div>
                    {book && <span className={classes.bookingMessage}>Din bokning har tagits emot. Tack för din bokning!</span>}
                </form >
            )
            }
        />
    )
}

export default BookingForm;