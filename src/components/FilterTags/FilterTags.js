import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React from 'react';
import { createUseStyles } from 'react-jss';
import { useDispatch, useSelector } from 'react-redux';
import { removeFilter } from '../../state/actions';

const useStyles = createUseStyles(theme => ({
    filterTags: {
        display: 'flex',
        justfyContent: 'center',
        alignItems: 'center',
        listStyle: 'none'
    },

    filterTagItem: {
        paddingLeft: theme.padding['2']
    },

    filterTagButton: {
        backgroundColor: theme.colors.white,
        border: `3px solid ${theme.colors.primary}`,
        padding: `${theme.padding['1']} ${theme.padding['2']}`,
        borderRadius: theme.borderRadius.xs
    },

    filterTagIcon: {
        paddingLeft: theme.padding['2']
    }
}));


const FilterTags = () => {
    const classes = useStyles();
    const filter = useSelector(state => state.filter)
    const dispatch = useDispatch();

    const handleRemoveFilter = key => {
        dispatch(removeFilter(key))
    }

    return (
        filter.active && <ul className={classes.filterTags} >
            {Object.entries(filter).filter(([key, value]) => key !== 'active' && value).map(([key, value]) => {
                value = typeof value === 'string' ? value : value.name;
                return (
                    <li className={classes.filterTagItem}>
                        <button className={classes.filterTagButton} onClick={() => handleRemoveFilter(key)}>
                            {value}
                            <FontAwesomeIcon className={classes.filterTagIcon} icon="times" size={'1x'} />
                        </button>
                    </li>
                )

            })}
        </ul>
    );
}

export default FilterTags;