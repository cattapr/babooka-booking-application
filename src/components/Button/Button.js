import React from 'react';
import { createUseStyles } from 'react-jss';


const useStyles = createUseStyles(theme => ({
    button: extraCSS => ({
        borderRadius: theme.borderRadius.m,
        cursor: 'pointer',
        fontWeight: theme.fontWeights.bold,
        borderWidth: 0,
        backgroundColor: extraCSS && extraCSS.backgroundColor,
        padding: extraCSS && extraCSS.padding,
        color: extraCSS && extraCSS.color,
    })
}));


const Button = ({ ariaLabelText, children, extraCSS, type = "button", ...props }) => {
    const classes = useStyles(extraCSS);
    return (
        <button aria-label={ariaLabelText} className={classes.button} type={type} {...props}>
            {children}
        </button>
    );
}

export default Button;