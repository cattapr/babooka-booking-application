import { Link } from '@reach/router';
import React from 'react';
import { createUseStyles } from 'react-jss';
import NavList from '../NavList';

const useStyles = createUseStyles(theme => ({
    header: {
        minHeight: theme.height.xs,
        backgroundColor: theme.colors.white,
        padding: `0 ${theme.padding['4']}`,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-between'
    },

    navigationLink: {
        textDecoration: 'none',
        color: theme.colors.primary,
        fontSize: theme.fontSizes.big,
        fontWeight: theme.fontWeights.extrabold
    }
}));

const Header = () => {
    const classes = useStyles();
    return (
        <header className={classes.header}>
            <h1>
                <Link className={classes.navigationLink} to="/">
                    Babooka
                </Link>
            </h1>
            <NavList />
        </header>
    );
}

export default Header;
