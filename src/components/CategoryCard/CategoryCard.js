import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React from 'react';
import { createUseStyles } from 'react-jss';
import { useDispatch } from 'react-redux';
import { setFilter } from '../../state/actions';


const useStyles = createUseStyles(theme => ({
    categoryButton: categoryColor => ({
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        width: theme.width.sm,
        height: theme.height.sm,
        borderRadius: theme.borderRadius.lg,
        boxShadow: theme.shadows.default,
        marginBottom: theme.margin.xs,
        background: theme.colors.white,
        border: 'none',
        color: categoryColor
    }),

    category: {
        marginRight: theme.margin['3'],
        textAlign: 'center',
        fontSize: theme.fontSizes.sm
    }
}));


const CategoryCard = ({ category }) => {
    const dispatch = useDispatch();
    const classes = useStyles(category.color);
    return (
        <li className={classes.category}>
            <button ariaLabelText="Välj en kategori" type="button" className={classes.categoryButton} onClick={() => {
                dispatch(setFilter({ category }))
            }}>
                <FontAwesomeIcon icon={category.icon} size={'3x'} color={category.color} />
            </button>
            {category.name}
        </li >
    )
}

export default CategoryCard;