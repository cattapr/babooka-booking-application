import React from 'react';
import { createUseStyles } from 'react-jss';
import theme from '../../theme/theme';
import NavItem from '../NavItem';

const useStyles = createUseStyles({

    navigationContainer: {
        display: 'flex',
        justifyContent: 'flex-end',
        alignItems: 'center',
        flexGrow: 1,
        minHeight: theme.height.xs
    },

    navigationList: {
        margin: 0,
        fontWeight: theme.fontWeights.semibold,
        fontSize: theme.fontSizes.lg
    }
});

const navigations = [
    { id: 1, path: '', title: 'Aktiviteter' },
    { id: 2, path: 'facilities', title: 'Anläggningar' },
    { id: 3, path: 'about-us', title: 'Om oss' },
    { id: 4, path: 'companies', title: 'För aktörer' },
]
const NavList = () => {
    const classes = useStyles();
    return (
        <nav className={classes.navigationContainer}>
            <ul className={classes.navigationList}>
                {navigations.map(navigation => {
                    return <NavItem key={navigation.id} navigation={navigation} />
                })}
            </ul>
        </nav>
    );
}

export default NavList;
