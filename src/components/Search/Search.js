
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React, { useState } from 'react';
import { createUseStyles, useTheme } from 'react-jss';
import { useDispatch } from 'react-redux';
import { setFilter } from '../../state/actions';
import theme from '../../theme/theme';
import Button from '../Button';


const styles = createUseStyles({
    formWrapper: {
        display: 'flex',
        height: theme.height.default,
        marginBottom: theme.margin['5'],
        position: 'relative'
    },

    input: {
        borderRadius: theme.borderRadius.m,
        flexGrow: 1,
        marginRight: theme.margin['3'],
        boxShadow: theme.shadows.md,
        backgroundColor: theme.colors.white,
        padding: `0 ${theme.padding['4']}`,
        borderWidth: 0,
        fontSize: theme.fontSizes.lg
    },

    searchIcon: {
        position: 'absolute',
        top: '50%',
        transform: 'translateY(-50%)',
        left: '1rem',
        color: theme.colors.primary
    }
});

const Search = () => {
    const classes = styles();
    const dispatch = useDispatch();
    const [searchValue, setSearchValue] = useState("");
    const theme = useTheme();

    const searchActivity = (event) => {
        event.preventDefault();
        dispatch(setFilter({ searchValue }))
    }
    return (
        <form className={classes.formWrapper} onSubmit={searchActivity}>
            <label htmlFor="search" className="srOnly">Sök efter aktivitet eller företag</label>
            <input id="search" className={classes.input} type="text" value={searchValue} placeHolder="Sök aktivitet eller företag.." onChange={(e) => setSearchValue(e.target.value)} />
            <FontAwesomeIcon icon="search" size={'2x'} className={classes.searchIcon} />
            <Button
                ariaLabelText="Sök efter en aktivitet"
                extraCSS={{
                    backgroundColor: theme.colors.white,
                    color: theme.colors.primary,
                    padding: `0 ${theme.padding['4']}`
                }} type="submit">Sök</Button>
        </form>
    );
};

export default Search;
