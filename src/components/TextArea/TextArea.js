import React from 'react';
import { Field } from 'react-final-form';
import { createUseStyles } from 'react-jss';

const useStyles = createUseStyles(theme => ({
    inputField: {
        padding: `${theme.padding['1']} ${theme.padding['2']}`,
        margin: theme.margin.xs,
        borderRadius: theme.borderRadius.m,
        boxShadow: theme.shadows.md,
        borderWidth: 0,
        flexGrow: 1
    },
}));

const TextArea = ({ name, placeholder, labelText }) => {
    const classes = useStyles();
    return (
        <Field name={name}>
            {({ input, meta }) => (
                <>
                    <label htmlFor={name} className="srOnly">{labelText}</label>
                    <textarea id={name} className={classes.inputField} {...input} placeholder={placeholder} />
                    {meta.touched && meta.error && <span>{meta.error}</span>}
                </>
            )}
        </Field>
    );
}

export default TextArea;