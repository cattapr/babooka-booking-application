import { ModalPortal, useBodyScrollLock, useTrapFocus } from "@weahead/react-customizable-modal";
import React from "react";
import { createUseStyles } from 'react-jss';

const useStyles = createUseStyles(theme => ({
    modalOverlay: {
        position: "fixed",
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
        overflowY: "scroll"
    },

    modalWrapper: {
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        width: theme.width.full,
        padding: 20,
        position: "absolute",
        backgroundColor: "rgba(0,0,0,0.5)"
    }
}));

export const CustomModal = ({ children }) => {
    const classes = useStyles();
    useBodyScrollLock();
    const modalRef = useTrapFocus();
    return (
        <ModalPortal id={`customModal`}>
            <div className={classes.modalOverlay}>
                <div ref={modalRef} className={classes.modalWrapper}>
                    {children}
                </div>
            </div>
        </ModalPortal>
    );
}

export default CustomModal;
