import React from 'react';
import { createUseStyles } from 'react-jss';
import BookingForm from '../BookingForm/BookingForm';
import Button from '../Button';
import InfoSection from './InfoSection';


const useStyles = createUseStyles(theme => ({
    modal: {
        background: theme.colors.white,
        borderRadius: theme.borderRadius['lg'],
        boxShadow: theme.shadows.md,
        width: theme.width.full,
        maxWidth: theme.width.contentWrapper,
        padding: `${theme.padding['3']} 0`
    },

    container: {
        margin: `${theme.margin['3']} 0`
    },

    closeModalButtonWrapper: {
        float: 'right',
        fontWeight: theme.fontWeights.bold,
        marginRight: theme.margin['2'],
        marginBottom: theme.margin['2']
    }
}));


const Modal = ({ setIsOpen, company, activity }) => {
    const classes = useStyles();

    return (
        <article className={classes.modal}>
            <header className={classes.closeModalButtonWrapper}>
                <Button ariaLabel="Stäng modalen" extraCSS={{
                    backgroundColor: 'transparent',
                }} onClick={() => setIsOpen(false)}>X</Button>
            </header>
            <main className={classes.container}>
                <InfoSection activity={activity} company={company} />
                <BookingForm activity={activity} />
            </main>
        </article>
    );
}

export default Modal;