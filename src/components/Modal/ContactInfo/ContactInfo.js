import React from 'react';
import { createUseStyles } from 'react-jss';
import InputField from '../../InputField';
import TextArea from '../../TextArea';


const useStyles = createUseStyles(theme => ({
    fieldset: {
        border: 'none'
    },

    fieldsWrapper: {
        display: 'grid',
        gridTemplateColumns: '2fr 1fr',
        gridTemplateAreas: `"input textarea"`,
        gridGap: '5rem',
        background: theme.colors.primary,
        padding: theme.padding['4']
    },

    inputFieldContainer: {
        display: 'flex',
        flexDirection: 'column',
        gridArea: 'input'
    },

    inputField: {
        padding: `${theme.padding['1']} ${theme.padding['2']}`,
        margin: theme.margin.xs,
        borderRadius: theme.borderRadius.m,
        boxShadow: theme.shadows.md,
        borderWidth: 0,
        flexGrow: 1
    },

    textareaFieldGridArea: {
        display: 'flex',
        gridArea: 'textarea',
        alignItems: 'stretch'

    },


}));

const ContactInfo = () => {
    const classes = useStyles();
    return (
        <fieldset className={classes.fieldset}>
            <legend className="srOnly">Dina kontaktuppgifter</legend>
            <div className={classes.fieldsWrapper}>
                <div className={classes.inputFieldContainer}>
                    <InputField
                        type="text"
                        name="name"
                        placeholder="Förnamn Efternamn"
                        labelText="Fyll in ditt namn och efternamn"
                        className="inputField"
                        required
                    />
                    <InputField
                        type="tel"
                        name="number"
                        placeholder="Telefon"
                        labelText="Fyll in ditt telefonnummer"
                        className="inputField"
                        required
                    />
                    <InputField
                        type="email"
                        name="email"
                        placeholder="Epostadress"
                        labelText="Fyll in din epostadress"
                        className="inputField"
                        required
                    />
                </div>
                <div className={classes.textareaFieldGridArea} >
                    <TextArea
                        name="note"
                        placeholder="Kommentar"
                        labelText="Skriv en kommentar"
                        className="inputField"
                    />
                </div>
            </div>
        </fieldset>
    );
}

export default ContactInfo;