import React from 'react';
import { createUseStyles } from 'react-jss';

const useStyles = createUseStyles(theme => ({
    articleInfo: {
        display: 'grid',
        gridTemplateColumns: '3fr 2fr',
        gridAutoRows: 'auto',
        gridGap: '5rem',
        listStyle: 'none',
        padding: `0 ${theme.padding['7']}`
    },

    content: {
        lineHeight: theme.lineHeight['sm'],
        fontSize: theme.fontSizes.lg,
        fontWeight: theme.fontWeights.normal,
        overflow: 'hidden'
    },

    contentIntro: {
        fontWeight: theme.fontWeights.thin
    },

    contentParagraph: {
        lineHeight: theme.lineHeight['sm'],
        margin: `${theme.margin['3']} 0`
    },

    contentImage: {
        width: '100%',
        maxHeight: '38rem',
        borderRadius: theme.borderRadius['lg'],
        boxShadow: theme.shadows.sm,
        objectFit: 'cover'
    },

    extraContent: {
        display: 'grid',
        gridTemplateColumns: '1fr 1fr',
    },
}));

const InfoSection = ({ activity, company }) => {
    const classes = useStyles();
    return (
        <section className={classes.articleInfo}>
            <div className={classes.content}>
                <h1>{activity.name}</h1>
                <span className={classes.contentIntro}>{company.name}, {company.city}</span>
                <p className={classes.contentParagraph}>{activity.description}</p>
                <div className={classes.extraContent}>
                    <span>{company.contact.phone}</span>
                    <span>{company.contact.website}</span>
                    <span>{company.contact.email}</span>
                    <span>{company.address} <br /> {company.postcode} {company.city}</span>
                </div>
            </div>
            <img className={classes.contentImage} src={activity.image.src} alt={activity.image.description} />
        </section>
    );
}

export default InfoSection;