import React, { useState } from 'react';
import { createUseStyles } from 'react-jss';
import CustomRadioButton from '../../CustomRadioButton';



const useStyles = createUseStyles(theme => ({
    selectFieldContainer: {
        display: 'grid',
        gridTemplateColumns: '2fr 1fr 1fr',
        gridGap: '2rem',
        margin: `${theme.margin['3']} 0`
    },

    selectField: {
        padding: `${theme.padding['1']} ${theme.padding['4']}`,
        borderRadius: theme.borderRadius.m,
        boxShadow: theme.shadows.md,
        borderWidth: 0
    },

    avaliableTimeSection: {
        margin: `${theme.margin['5']} ${theme.margin['5']}`,
        borderTop: `${theme.borderWidths['2']} solid ${theme.colors.primary3}`,
        padding: `${theme.padding['4']} ${theme.padding.lg} 0  ${theme.padding.lg}`
    },

    selectFieldHeading: {
        textAlign: 'center',
        fontSize: theme.fontSizes.medium
    },

    fieldset: {
        border: 'none'
    },

    avaliableTimeHeader: {
        textAlign: 'center',
        fontSize: theme.fontSizes.lg,
        fontWeight: theme.fontWeights.bold
    },

    avaliableTimeList: {
        display: 'grid',
        gridTemplateColumns: 'repeat(5, 1fr)',
        gridGap: '2rem',
        listStyle: 'none',
        padding: 0,
        paddingTop: theme.padding['3'],
        borderTop: `${theme.borderWidths['2']} solid ${theme.colors.primary3}`,
    }
}));

const getBookableTimes = (bookableTimes) => {
    const dates = {};

    bookableTimes.forEach(time => {
        dates[time.date] = dates[time.date] ? [...dates[time.date], time] : [time];
    })

    return dates;
}

const getDateString = (time) => {
    return new Date(time).toLocaleDateString('sv-SE', {
        weekday: 'short',
        year: 'numeric',
        month: 'long',
        day: 'numeric',
    });
}
const AvailableTimeSection = ({ activity }) => {
    const bookableTimes = getBookableTimes(activity.bookableTimes);
    const [bookingDate, setBookingDate] = useState(activity.bookableTimes[0].date);
    const classes = useStyles();

    const handleChangeSelectBookingDate = (event) => {
        setBookingDate(event.target.value)
    }
    return (
        <section className={classes.avaliableTimeSection}>
            <fieldset className={classes.fieldset}>
                <legend className={classes.selectFieldHeading}>Lediga tider</legend>
                <div className={classes.selectFieldContainer}>
                    <select value={bookingDate} name="date" id="date-select" className={classes.selectField} onChange={handleChangeSelectBookingDate}>
                        {Object.keys(bookableTimes).map(time => (
                            <option key={time} value={time}>
                                {getDateString(time)}
                            </option>
                        ))}
                    </select>

                    <select name="duration" id="duration-select" className={classes.selectField}>
                        <option value="">--Välj tidslängd--</option>
                        <option value="">1 timmar</option>
                        <option value="">2 timmar</option>
                    </select>
                    <select name="persons" id="persons-select" className={classes.selectField}>
                        <option value="">--Välj antal personer--</option>
                        <option value="">1</option>
                        <option value="">2</option>
                    </select>
                </div>
            </fieldset>
            {bookingDate &&
                <fieldset className={classes.fieldset}>
                    <legend className={classes.avaliableTimeHeader}>{getDateString(bookingDate)}</legend>
                    <ul className={classes.avaliableTimeList}>
                        {bookableTimes[bookingDate].map(booking => {
                            return (
                                <li key={booking.id}>
                                    <CustomRadioButton
                                        type="radio"
                                        name={"availableTime"}
                                        value={`${booking.date}: ${booking.startTime} - ${booking.endTime}`}
                                        booking={booking}
                                        required
                                    />
                                </li>
                            )
                        })
                        }
                    </ul>
                </fieldset>
            }

        </section >
    );
}

export default AvailableTimeSection;