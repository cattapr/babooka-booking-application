import { Link } from '@reach/router';
import React from 'react';
import { createUseStyles, useTheme } from 'react-jss';

const useStyles = createUseStyles(theme => ({
    navigationItem: {
        display: 'inline-block',
        margin: `0 ${theme.margin['2']}`,
    },
    navigationLink: {
        textDecoration: 'none',
        color: theme.colors.black,
        fontSize: theme.fontSizes.lg,
    }
}));


const NavItem = ({ navigation }) => {
    const theme = useTheme();
    const classes = useStyles({ theme });
    return <li className={classes.navigationItem}><Link className={classes.navigationLink} to={`/${navigation.path}`}>{navigation.title}</Link></li>
}

export default NavItem;
