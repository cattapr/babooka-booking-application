import React, { useState } from 'react';
import { createUseStyles, useTheme } from 'react-jss';
import { useSelector } from 'react-redux';
import theme from '../../theme/theme';
import Button from '../Button';
import CustomModal from '../CustomModal/CustomModal';
import Modal from '../Modal';

const useStyles = createUseStyles({

    card: {
        backgroundColor: theme.colors.white,
        borderRadius: theme.borderRadius.lg,
        boxShadow: theme.shadows.lg,
        overflow: 'hidden'
    },

    imageContainer: {
        position: 'relative'
    },

    imageCategory: categoryColor => ({
        position: 'absolute',
        top: '1.5rem',
        right: '1.5rem',
        padding: `${theme.padding.xs} ${theme.padding.default}`,
        borderRadius: theme.borderRadius.xs,
        fontSize: theme.fontSizes.xs,
        fontWeight: theme.fontWeights.bold,
        color: theme.colors.white,
        backgroundColor: categoryColor
    }),

    cardImage: {
        width: theme.width.full,
        height: theme.height.md,
        fontSize: theme.fontSizes.sm,
        objectFit: 'cover'
    },

    cardContent: {
        padding: `${theme.padding.md} ${theme.padding['2']}`,
        color: theme.colors.primary2,
        lineHeight: theme.lineHeight.sm,
        fontSize: theme.fontSizes.sm
    },

    cardContentHeading: {
        fontWeight: theme.fontWeights.bold,
        color: theme.colors.black,
        fontSize: theme.fontSizes.sm,
        padding: `${theme.padding['1']} 0`
    },

    cardContentDescription: {
        display: 'flex',
        flexDirection: 'column',
    },

    cardExtraContentDescription: {
        padding: `${theme.padding['default']} 0`,
        fontSize: theme.fontSizes.xs
    },

    cardBookingDescription: {
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
        borderTop: `${theme.borderWidths['default']} solid ${theme.colors.primary3}`,
        paddingTop: theme.padding['2'],
        fontWeight: theme.fontWeights.thin,
        fontSize: theme.fontSizes.xs
    }
});

const ActivityCard = ({ activity }) => {
    const [isOpen, setIsOpen] = useState(false);
    const { companies } = useSelector(state => state.companies);
    const { categories } = useSelector(state => state.categories);
    const company = companies.find(company => company.id === activity.companyId)
    const category = categories.find(category => category.id === activity.categoryIds[0])
    const classes = useStyles(category.color);
    const theme = useTheme();
    return (
        <>
            {isOpen && <CustomModal>
                <Modal activity={activity} company={company} setIsOpen={setIsOpen} />
            </CustomModal>}
            <li>
                <article className={classes.card}>
                    <header className={classes.imageContainer}>
                        <span className={classes.imageCategory}>{category.name}</span>
                        <img className={classes.cardImage} src={activity.image.src} alt={activity.image.description} />
                    </header>
                    <main className={classes.cardContent}>
                        <span className={classes.cardContentHeading}>{activity.name}</span>
                        <section className={classes.cardContentDescription}>
                            <span>{company.name}</span>
                            <span>{company.city}</span>
                            <span className={classes.cardExtraContentDescription}>
                                Första lediga tid: {activity.bookableTimes[0].date}
                            </span>
                        </section>

                        <section className={classes.cardBookingDescription}>
                            <span> fr.{activity.price} kr</span>
                            <Button ariaLabelText="Boka aktivitet" extraCSS={{
                                backgroundColor: theme.colors.primary,
                                padding: `${theme.padding.sm} ${theme.padding.md}`,
                                color: theme.colors.white
                            }} onClick={() => setIsOpen(true)}>Boka</Button>
                        </section>
                    </main>
                </article>
            </li>
        </>
    );
}

export default ActivityCard;
