import React from 'react';
import { createUseStyles } from 'react-jss';
import theme from '../../theme/theme';
import Search from '../Search/';

const styles = createUseStyles({
    hero: {
        display: 'flex',
        backgroundColor: theme.colors.primary,
        flexDirection: 'column',
        height: theme.height.lg,
        justifyContent: 'center',
        alignItems: 'center'
    },

    container: {
        width: theme.width.full,
        maxWidth: theme.width.lg
    },

    heading: {
        marginBottom: theme.margin['5'],
        fontSize: theme.fontSizes.xl,
        textAlign: 'center'
    }
});

const Hero = () => {
    const classes = styles();
    return (
        <section className={classes.hero}>
            <div className={classes.container}>
                <h2 className={classes.heading}>Boka din aktivitet</h2>
                <Search />
            </div>
        </section>
    );
}

export default Hero;
