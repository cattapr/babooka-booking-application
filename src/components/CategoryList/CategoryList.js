import React, { useEffect } from 'react';
import { createUseStyles } from 'react-jss';
import { useDispatch, useSelector } from 'react-redux';
import { fetchCategories } from '../../state/actions/';
import CategoryCard from '../CategoryCard';

const useStyles = createUseStyles({

    categoryContainer: {
        marginTop: '-5rem'
    },

    categoryList: {
        listStyle: 'none',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        padding: 0,
        margin: 0
    }
});

const CategoryList = () => {
    const classes = useStyles();
    const { categories, loading } = useSelector(state => state.categories);
    const dispatch = useDispatch();

    useEffect(
        () => {
            if (!loading && !categories.length) {
                dispatch(fetchCategories());
            }
        },
        [categories, loading, dispatch]
    );

    return (
        <section className={classes.categoryContainer}>
            <ul className={classes.categoryList}>
                {categories.map(category => {
                    return <CategoryCard key={category.id} category={category} />
                })}
            </ul>
        </section>
    );
}

export default CategoryList;
