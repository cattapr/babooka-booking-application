import React from 'react';
import { createUseStyles } from 'react-jss';

const useStyles = createUseStyles(theme => ({
    footer: {
        minHeight: theme.height.md,
        backgroundColor: theme.colors.primary5
    }
}));

const Footer = () => {
    const classes = useStyles();
    return (
        <footer className={classes.footer}></footer>
    );
}

export default Footer;
